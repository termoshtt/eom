extern crate eom;
extern crate ndarray;

use eom::traits::*;
use eom::*;
use ndarray::array;

fn main() {
    let dt = 0.01;
    let eom = ode::Lorenz63::default();
    let mut teo = explicit::RK4::new(eom, dt);

    let mut x = adaptor::iterate(&mut teo, array![1.0, 0.0, 0.0], 100);
    let mut y = 1e-2 * array![1.0, 0.0, 0.0] + &x;
    println!("time,x1,x2,x3,y1,y2,y3");
    for t in 0..1800 {
        teo.iterate(&mut x);
        teo.iterate(&mut y);
        println!(
            "{},{},{},{},{},{},{}",
            t as f64 * dt,
            x[0],
            x[1],
            x[2],
            y[0],
            y[1],
            y[2]
        );
    }
}
