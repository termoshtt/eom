#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set nospell:

import pandas as pd
from subprocess import check_call

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def exec(name):
    result_fn = name + ".csv"
    with open(result_fn, "w") as f:
        check_call(["cargo", "run", "--release", "--example", name], stdout=f)
    return pd.read_csv(result_fn).set_index("time")


def lorenz63():
    data = exec("lorenz63")[1:]
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot(xs=data["x"], ys=data["y"], zs=data["z"])
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    plt.savefig("src/lorenz63.png", transparent=True)


def unstable_orbit():
    data = exec("unstable_orbit")
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot(xs=data["x1"], ys=data["x2"], zs=data["x3"])
    ax.plot(xs=data["y1"], ys=data["y2"], zs=data["y3"])
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    plt.savefig("src/unstable_orbit.png", transparent=True)


if __name__ == '__main__':
    lorenz63()
    unstable_orbit()
