Gram-Schmit vector
==================
To analyze the dynamics of **any** small vector \\( \delta \\),
we consider the dynamics of a basis \\( Q_n = ( e_1 e_2 \ldots e_N ) \in \mathcal{R}^{N \times N} \\)
\\[
  Q^\prime_{n+1} = J_n Q_n
\\]
Since \\( J_n \\) has extending and decreasing directions, \\( Q^\prime_{n+1} \\) becomes non-unitary.
Let us use QR-decomposition to get a new basis:
\\[
  Q_{n+1} R_{n+1} = Q^\prime_{n+1},
\\]
and we obtain an iteration:
\\[
J_m Q_m = Q_{m+1} R_{m+1}.
\\]
Because \\( \delta_m \\) can be expressed by these basis \\( Q_m \\) with a coefficient \\( d_m \in \mathcal{T} \equiv \mathcal{R}^N \\)
\\[
  \delta_m = Q_m d_m,
\\]
its dynamics can be rewritten into the dynamics of its coefficient:
\\[
  d_{m+1} = R_m d_m.
\\]
Here we obtain a commutation diagram:
\\[
\require{AMScd}
\begin{CD}
T_{n-1} \mathcal{M} @>{J_{n-1}}>> T_n \mathcal{M} @>{J_n}>> T_{n+1} \mathcal{M} \\\\
@VV{Q_{n-1}^T}V @VV{Q_n^T}V @VV{Q_{n+1}^T}V \\\\
\mathcal{T} @>>{R_{n-1}}> \mathcal{T} @>>{R_n}> \mathcal{T} \\\\
\end{CD}
\\]

We can calculate the Lyapunov exponent from this forward iteration:
\\[
\lambda_i = \lim_{m \to \infty} \frac{1}{m} \sum_{n=1}^m \log {\left(R_n\right)}_{ii}.
\\]
The orbit \\( x_n \\) is stable if \\( | \lambda_i | < 1 \\) for all \\( i \\), and otherwise unstable.
We call each column of \\( Q_m \\) "Gram-Schmit vector" or "Lyapunov vector".
