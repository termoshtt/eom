はじめに
==============

これは常微分方程式ソルバー及び力学系解析のためのライブラリである[eom crate]について
数学的な側面を補足するためのノートです。

[rustdoc]: https://termoshtt.gitlab.io/eom/doc/eom/
[eom crate]: https://crates.io/crates/eom

Contributing to the book
-------------------------
このノートはCreative Commons Attribution-NonCommercial-ShareAlike 4.0 International License ([CC BY-NC-SA 4.0])でライセンスされています。
問題があれば[GitLabのeom repository][GitLab]に報告してください。日本語でももちろん構いません。

[![License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

[CC BY-NC-SA 4.0]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[GitLab]: https://gitlab.com/termoshtt/eom
