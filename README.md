Equation of Motions
====================

Configurable ODE/PDE solver

Documents
----------
- [docs.rs](https://docs.rs/eom)
- [rustdoc for master branch](https://termoshtt.gitlab.io/eom/doc/eom)
- [Book for mathematical aspects](https://termoshtt.gitlab.io/eom/book)

Contents
---------
- Algorithms
  - explicit schemes
    - Euler
    - Heun
    - classical 4th order Runge-Kutta
  - semi-implicit schemes
    - stiff RK4
- ODE
  - [Lorenz three-variables system](https://en.wikipedia.org/wiki/Lorenz_system)
  - [Lorenz 96 system](https://en.wikipedia.org/wiki/Lorenz_96_model)
  - [Roessler system](https://en.wikipedia.org/wiki/R%C3%B6ssler_attractor)
  - GOY shell model
    - [notebook](GOY.ipynb)
- PDE
  - Kuramoto-Sivashinsky equation
    - [notebook](KSE.ipynb)

Lyapunov analysis
-----------------
- [Lyapunov expoents of Lorenz 63 model](http://sprott.physics.wisc.edu/chaos/lorenzle.htm)
  - [example](examples/lyapunov.rs)
- [Covarient Lyapunov vector (CLV)](https://arxiv.org/abs/1212.3961)
  - [example](examples/clv.rs) 
  - [notebook](CLV.ipynb)


License
-------
- MIT-License, see [LICENSE](LICENSE) file.
- Book is published as [CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/)