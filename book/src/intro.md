Introduction
==============

Welcome to eom-book!

This is an appendix of [rustdoc] of [eom crate] focused on the mathematical aspects.

[rustdoc]: https://termoshtt.gitlab.io/eom/doc/eom/
[eom crate]: https://crates.io/crates/eom

Contributing to the book
-------------------------
This book is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License ([CC BY-NC-SA 4.0]).
Any types of contributions are welcome, please don’t hesitate to file an issue or send a pull request on [GitLab].

[![License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

[CC BY-NC-SA 4.0]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[GitLab]: https://gitlab.com/termoshtt/eom