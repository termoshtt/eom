# Lyapunov Analysis

Concept
-------
The behavior of a linear dynamics
\\[
    x_{n+1} = A x_n
\\]
is dominated by the eigenvalues \\( \lambda_i \\) of the matrix \\( A \\).
If \\( |\lambda_i| < 1 \\) for all \\( i \\), the fixed point \\( x = 0 \\) is stable, and otherwise unstable.
Lyapunov analysis extends this stability analysis into orbits in nonlinear dynamics.

Notations
----------
- State \\( x_n \in \mathcal{R}^N \\), \\( n \\) suffix denotes time, and \\( N \\) denotes the dimension of states.
  - orbit \\( \\{ x_n \| n \in \mathcal{Z} \\} \\)
- Time-evolution function \\( F: \mathcal{R}^N \to \mathcal{R}^N, C^1 \\) s.t.
\\[
x_{n+1} = F(x_n)
\\]
- Attractor \\( \mathcal{M} \subset \mathcal{R}^N \\)
\\[
    F(\mathcal{M}) \subset \mathcal{M}
\\]
- Jacobi matrix \\( J_x \\) at \\( x \in \mathcal{M} \\) s.t.
\\[
    J_x v = \lim_{\alpha \to 0} \frac{F(x + \alpha v) - F(x)}{\alpha}
\\]
  - Use \\( J_n \\) abbreviation for \\( J_{x_n} \\)

- Tangent space \\( T_x \mathcal{M} \\) of Attractor \\( \mathcal{M} \\) at \\( x \in \mathcal{A} \\)
  - Use \\(T_n \mathcal{M} \\) abbreviation for \\( T_{x_n} \mathcal{M} \\)
  - \\( J_n \\) maps \\( T_n \mathcal{M} \\) into \\( T_{n+1} \mathcal{M} \\)

Stablity of orbits
-------------------

Let us consider a case where we add a small noise \\( x_n \mapsto x_n + \delta \\) at time \\( n \\)
and its modified orbit
\\[
  y_m = f^{(m-n)}(x_n + \delta),
\\]
where \\( (m = n, n+1, \\cdots )\\).
If \\( y_n \\) convergent into \\( x_n \\) for any small \\( \delta \in \mathcal{R}^n \\), i.e.
\\[
  \| x_n - y_n \| \to 0
\\]
we call this orbit is stable, and otherwise unstable.

Using linear approximation of \\( f \\), we get a linear dynamical system of the deviation \\( \delta_n \equiv y_n - x_n \\):
\\[
  \delta_{n+1} = J_n \delta_n.
\\]
If \\( J_n \\) is temporary-constant, i.e. \\( J_n \equiv J \\), it is nothing but an ordinal linear dynamical system,
and its behavior is dominated by the eigenvalue of \\( J \\).
Lyapunov analysis extends this discussion for general series of \\( J_n \\).

Gram-Schmit vector
-------------------
To analyze the dynamics of **any** small vector \\( \delta \\),
we consider the dynamics of a basis \\( Q_n = ( e_1 e_2 \ldots e_N ) \in \mathcal{R}^{N \times N} \\)
\\[
  Q^\prime_{n+1} = J_n Q_n
\\]
Since \\( J_n \\) has extending and decreasing directions, \\( Q^\prime_{n+1} \\) becomes non-unitary.
Let us use QR-decomposition to get a new basis:
\\[
  Q_{n+1} R_{n+1} = Q^\prime_{n+1},
\\]
and we obtain an iteration:
\\[
J_m Q_m = Q_{m+1} R_{m+1}.
\\]
Because \\( \delta_m \\) can be expressed by these basis \\( Q_m \\) with a coefficient \\( d_m \in \mathcal{T} \equiv \mathcal{R}^N \\)
\\[
  \delta_m = Q_m d_m,
\\]
its dynamics can be rewritten into the dynamics of its coefficient:
\\[
  d_{m+1} = R_m d_m.
\\]
Here we obtain a commutation diagram:
\\[
\require{AMScd}
\begin{CD}
T_{n-1} \mathcal{M} @>{J_{n-1}}>> T_n \mathcal{M} @>{J_n}>> T_{n+1} \mathcal{M} \\\\
@VV{Q_{n-1}^T}V @VV{Q_n^T}V @VV{Q_{n+1}^T}V \\\\
\mathcal{T} @>>{R_{n-1}}> \mathcal{T} @>>{R_n}> \mathcal{T} \\\\
\end{CD}
\\]

We can calculate the Lyapunov exponent from this forward iteration:
\\[
\lambda_i = \lim_{m \to \infty} \frac{1}{m} \sum_{n=1}^m \log {\left(R_n\right)}_{ii}.
\\]
The orbit \\( x_n \\) is stable if \\( | \lambda_i | < 1 \\) for all \\( i \\), and otherwise unstable.
We call each column of \\( Q_m \\) "Gram-Schmit vector" or "Lyapunov vector".